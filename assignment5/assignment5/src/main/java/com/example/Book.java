package com.example;
import java.sql.*;

public class Book {
    private String isbn;
    private String title;
    private Date pubdate;
    private int pubid;
    private double cost;
    private double retail;
    private double discount;
    private String category;
    private Publisher publisher;

    public Book(String isbn, String title, Date pubdate, int pubid, double cost, double retail, double discount, String category, Publisher publisher) {
        this.isbn = isbn;
        this.title = title;
        this.pubdate = pubdate;
        this.pubid = pubid;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
        this.publisher = publisher;
    }
    public String getIsbn() {
        return this.isbn;
    }
    public String getTitle() {
        return this.title;
    }
    public Date getPubDate() {
        return this.pubdate;
    }
    public int getPubId() {
        return this.pubid;
    }
    public double getCost() {
        return this.cost;
    }
    public double getRetail() {
        return this.retail;
    }
    public double getDiscount() {
        return this.discount;
    }
    public String getCategory() {
        return this.category;
    }
    public Publisher getPublisher() {
        return this.publisher;
    }
    @Override
    public String toString() {
        return "Isbn: " + isbn + " title: " + title + " date: " + pubdate + " pubid: " + pubid + " cost: " + cost + " retail: " + retail + " discount: " + discount + " category: " + category;
    }
}

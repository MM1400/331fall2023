package com.example;
import java.sql.*;
import java.io.*;

public class JustLeeServices {

    public static Connection getConnection(String url, String user, String pass) {
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return con;
    }
        public static Book getBook(String isbn, Connection con) {
        Book b = null;
        Publisher p = null;
        try{
        PreparedStatement statement = con.prepareStatement("SELECT * FROM publisher INNER JOIN books ON publisher.pubid = books.pubid WHERE isbn = ?");
        statement.setString(1, isbn);
        ResultSet result = statement.executeQuery();
        while(result.next()) {
            int pubid = result.getInt("pubid");
            String name = result.getString("name");
            String contact = result.getString("contact");
            String phone = result.getString("phone");
            p = new Publisher(pubid, name, contact, phone);
        }
        statement = con.prepareStatement("SELECT * FROM books WHERE isbn=?");
        statement.setString(1, isbn);
        result = statement.executeQuery();
        while(result.next()) {
            String title = result.getString("title");
            Date pubdate = result.getDate("pubdate");
            int pubid = result.getInt("pubid");
            double cost = result.getDouble("cost");
            double retail = result.getDouble("retail");
            double discount = result.getDouble("discount");
            String category = result.getString("category");
            b = new Book(isbn, title, pubdate, pubid, cost, retail, discount, category, p);
        }
        statement.close();
        con.close();
        } catch(SQLException e) {
            System.out.println(e);
        }
        return b;
    }
    public static void addBook(Book b, Connection con) {
        try {
            PreparedStatement statement = con.prepareStatement("INSERT INTO books(isbn, title, pubdate, pubid, cost, retail, discount, category) VALUES(?,?,?,?,?,?,?, ?)");
            statement.setString(1, b.getIsbn());
            statement.setString(2, b.getTitle());
            statement.setDate(3, b.getPubDate());
            statement.setInt(4, b.getPubId());
            statement.setDouble(5, b.getCost());
            statement.setDouble(6, b.getRetail());
            statement.setDouble(7, b.getDiscount());
            statement.setString(8, b.getCategory());


            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    public static void main(String[] args) {

        Console console = System.console();
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        String user = console.readLine("Enter user : ");
        String pass = String.valueOf(console.readPassword("Enter password : "));
        Connection con = getConnection(url, user, pass);

        Date d = Date.valueOf("2017-12-03");
        Book b = new Book("1000000000", "TEST BOOK", d, 1, 20, 40, 0, "TEST", null);
        JustLeeServices.addBook(b, con);
        System.out.println(JustLeeServices.getBook("1000000000", con));
    }
}
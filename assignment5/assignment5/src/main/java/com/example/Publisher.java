package com.example;

public class Publisher {
    private int pubid;
    private String name;
    private String contact;
    private String phone;

    public Publisher(int pubid, String name, String contact, String phone) {
        this.pubid = pubid;
        this.name = name;
        this.contact = contact;
        this.phone = phone;
    }

    public int getPubid() {
        return pubid;
    }

    public void setPubid(int pubid) {
        this.pubid = pubid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
}